// $(function(){
// 	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
// 	ua = navigator.userAgent,

// 	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

// 	scaleFix = function () {
// 		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
// 			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
// 			document.addEventListener("gesturestart", gestureStart, false);
// 		}
// 	};
	
// 	scaleFix();
// });
// var ua=navigator.userAgent.toLocaleLowerCase(),
//  regV = /ipod|ipad|iphone/gi,
//  result = ua.match(regV),
//  userScale="";
// if(!result){
//  userScale=",user-scalable=0"
// }
// document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')

// ЕСЛИ ПРОЕКТ РЕСПОНСИВ ТО ВСЕ ЧТО ВЫШЕ НУЖНО РАССКОМЕНТИРОВАТЬ. СКРИПТ ВЫШЕ ПРЕДНАЗНАЧЕН ДЛЯ КОРРЕКТНОГО ОТОБРАЖЕНИЯ ВЕРСТКИ ПРИ СМЕНЕ ОРИЕНТАЦИИ НА ДЕВАЙСАХ



// INCLUDE FUNCTION
// ниже пример подключения табов. То есть создаем переменную с селектором на который нужно выполнить инициализацию, потом условием проверяем есть ли такой селектор, если есть то скрипт подключит файл с помощью функции INCLUDE. Это важно так как в наших проектах может быть подключено по 20 библиотек это оочеь плохо так как проект становится тяжелым и переполненным мусором

var sticky 		= $(".sticky"),
	matchheight = $("[data-mh]"),
	popup 		= $("[data-popup]"),
	owl 		= $(".owl-carousel"),
	rangeSlider = $(".slider-range"),
	styler 		= $(".styler"),
	accordion   = $("#accordion"),
	tabs   		= $("#horizontalTab"),
	fancyBox 	= $(".fancybox");

// if(tabsBox.length){
//   include("js/easyResponsiveTabs.js");
// }

	if($(fancyBox).length){
	  include("js/jquery.fancybox.js");
	}
	if(tabs.length){
	  include("js/jquery.responsiveTabs.min.js");
	}
	if($(owl).length){
	  include("js/owl.carousel.js");
	}
	if(matchheight.length){
		include("js/jquery.matchHeight-min.js");
	}
	if(popup.length){
		include('js/jquery.arcticmodal.js');
	}
	if(sticky.length){
		include("js/sticky.js");
		include("js/jquery.smoothscroll.js");
	}
	if(rangeSlider.length){
		include('js/jquery-ui.js');
		include('js/jquery.ui.touch-punch.min.js');
	}
	if(styler.length){
		include("js/formstyler.js");
	}
// Плагин скриптом задает высоту блоку и выставляет по самому большому , нужно чтобы на блоке висел класс (data-mh) и атрибут (data-mh) с одним названием для одинаковых блоков, тогда он будет работать, выше находиться его инициализация, еще он хорошо работает при респонзиве. 

function include(url){ 
  document.write('<script src="'+ url + '"></script>'); 
}




$(document).ready(function(){

  $(".menu_link").on("click", function(){
  	$("body").addClass("navTrue");
  })
  $(".menu_sub").on("mouseleave", function(){
		$("body").removeClass("navTrue");
	})

  	/* ------------------------------------------------
	FANCYBOX START
	------------------------------------------------ */

		if (fancyBox.length){
			fancyBox.fancybox({

			});
		}

	/* ------------------------------------------------
	FANCYBOX END
	------------------------------------------------ */

	/* ------------------------------------------------
	TABS START
	------------------------------------------------ */

		if (tabs.length){
			tabs.responsiveTabs({
				startCollapsed: 'accordion'
			});
		}

	/* ------------------------------------------------
	TABS END
	------------------------------------------------ */

	/* ------------------------------------------------
	CAROUSEL START
	------------------------------------------------ */

		if(owl.length){
			var sync1 = $("#sync1");
  			var sync2 = $("#sync2");

			sync1.owlCarousel({
				items : 1,
			    singleItem : true,
			    slideSpeed : 1000,
			    navigation: false,
			    pagination:false,
			    afterAction : syncPosition,
			    responsiveRefreshRate : 200,
			});
			 
			sync2.owlCarousel({
			items : 4,
			pagination:false,
			responsiveRefreshRate : 100,
			afterInit : function(el){
			  el.find(".owl-item").eq(0).addClass("synced");
			}
			});
		}

		function syncPosition(el){
				var current = this.currentItem;
				$("#sync2")
				  .find(".owl-item")
				  .removeClass("synced")
				  .eq(current)
				  .addClass("synced")
				if($("#sync2").data("owlCarousel") !== undefined){
				  center(current)
				}
			}
			 
			$("#sync2").on("click", ".owl-item", function(e){
				e.preventDefault();
				var number = $(this).data("owlItem");
				sync1.trigger("owl.goTo",number);
			});
			 
			function center(number){
				var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
				var num = number;
				var found = false;
				for(var i in sync2visible){
				  if(num === sync2visible[i]){
				    var found = true;
				  }
				}

				if(found===false){
				  if(num>sync2visible[sync2visible.length-1]){
				    sync2.trigger("owl.goTo", num - sync2visible.length+2)
				  }else{
				    if(num - 1 === -1){
				      num = 0;
				    }
				    sync2.trigger("owl.goTo", num);
				  }
				} else if(num === sync2visible[sync2visible.length-1]){
				  sync2.trigger("owl.goTo", sync2visible[1])
				} else if(num === sync2visible[0]){
				  sync2.trigger("owl.goTo", num-1)
				}

			}

		



	/* ------------------------------------------------
	CAROUSEL END
	------------------------------------------------ */

	/* ------------------------------------------------
	POPUP START
	------------------------------------------------ */

		if(popup.length){
			popup.on('click',function(){
			    var modal = $(this).data("popup");
			    $(modal).arcticmodal({
			    		beforeOpen: function(){
			    	}
			    });
			});
		};

	/* ------------------------------------------------
	POPUP END
	------------------------------------------------ */

	/* ------------------------------------------------
	STICKY START
	------------------------------------------------ */

		if (sticky.length){
			$(sticky).sticky({
		        topspacing: 0,
		        styler: 'is-sticky',
		        animduration: 0,
		        unlockwidth: false,
		        screenlimit: false,
		        sticktype: 'alonemenu'
			});
		};

	/* ------------------------------------------------
	STICKY END
	------------------------------------------------ */


	/* ------------------------------------------------
	RANGE-SLIDER START
	------------------------------------------------ */

		if(rangeSlider.length){

			rangeSlider.slider({
				min: 0,
				max: 400,
				value: 150,
                orientation: "horizontal"
			});  
			rangeSlider.slider();  

			var rangeSlider_h = $(".range_h");
			rangeSlider_h.slider({
				min: 0,
				max: 400,
				value: 150,
                slide: function( event, ui ) {
	                $( "#amount1" ).val( ui.value );
	            }	
			});  
			$( "#amount1" ).on( "change", function() {
		      rangeSlider_h.slider( "value", this.value );
		    });
			rangeSlider_h.slider();  


			var rangeSlider1 = $(".range1");
			rangeSlider1.slider({
				min: 0,
				max: 400,
				value: 150,
                slide: function( event, ui ) {
	                $( "#amount1" ).val( ui.value );
	            }	
			});  
			$( "#amount1" ).on( "change", function() {
		      rangeSlider1.slider( "value", this.value );
		    });
			rangeSlider1.slider();  


			var rangeSlider2 = $(".range2");
			rangeSlider2.slider({
				min: 2.2,
				max: 5,
				step: 0.01,
				value: 3,
                slide: function( event, ui ) {
	                $( "#amount2" ).val( ui.value );
	            }	
			});  
			$( "#amount2" ).on( "change", function() {
		      rangeSlider2.slider( "value", this.value );
		    });
			rangeSlider2.slider();  


			var rangeSlider3 = $(".range3");
			rangeSlider3.slider({
				min: 1,
				max: 50,
				value: 15,
                slide: function( event, ui ) {
	                $( "#amount3" ).val( ui.value );
	            }	
			});  
			$( "#amount3" ).on( "change", function() {
		      rangeSlider3.slider( "value", this.value );
		    });
			rangeSlider3.slider();  
	    }


	/* ------------------------------------------------
	RANGE-SLIDER END
	------------------------------------------------ */

	/* ------------------------------------------------
	FORMSTYLER START
	------------------------------------------------ */

		if (styler.length){
			styler.styler({
				
			});
		}

	/* ------------------------------------------------
	FORMSTYLER END
	------------------------------------------------ */


	/* ------------------------------------------------
	ACCORDION START
	------------------------------------------------ */

			if($('.accordion_item_link').length){
				$('.accordion_item_link').on('click', function(){
					$(this)
					.toggleClass('active')
					.next('.sub-menu')
					.slideToggle()
					.parents(".accordion_item")
					.siblings(".accordion_item")
					.find(".accordion_item_link")
					.removeClass("active")
					.next(".sub-menu")
					.slideUp();
				});  
			}
				$('.accordion_item_link').on('click', function(e) {
				    e.preventDefault();
				});


	/* ------------------------------------------------
	ACCORDION END
	------------------------------------------------ */

		

});